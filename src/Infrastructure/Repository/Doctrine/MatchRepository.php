<?php

namespace App\Infrastructure\Repository\Doctrine;

use App\Domain\Match;
use App\Domain\Repository\MatchRepositoryInterface;

class MatchRepository implements MatchRepositoryInterface
{
    public function findByMatchIdAndSportType(string $sportType, string $matchId): ?Match
    {
        return null;
    }

    public function save(Match $match): void
    {
    }
}