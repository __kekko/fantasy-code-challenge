<?php

namespace App\Infrastructure;

use App\Application\Command\AcceptMatchCommand;
use App\Application\MatchFactoryInterface;
use App\Domain\Match;
use App\Domain\Team;
use App\Infrastructure\Service\FeedClient;
use App\Infrastructure\Service\FeedClientInterface;
use App\Infrastructure\Service\FeedParser;

class MatchFactory implements MatchFactoryInterface
{
    /**
     * @var FeedClient
     */
    private $feedClient;

    /**
     * @var FeedParser
     */
    private $feedParser;

    public function __construct(FeedClientInterface $feedClient, FeedParser $feedParser)
    {
        $this->feedClient = $feedClient;
        $this->feedParser = $feedParser;
    }

    public function createMatch(AcceptMatchCommand $command): Match
    {
        $feed = $this->feedClient->retrieveFeed($command->feedFile);
        $matchStats = $this->feedParser->parse($feed);

        $homeTeam = new Team($command->teams['home'], $matchStats->getHomePLayers());
        $awayTeam = new Team($command->teams['away'], $matchStats->getAwayPLayers());

        $match = new Match(
            $command->competition,
            $command->matchId,
            $command->season,
            $command->sport,
            $matchStats->getMatchLengthInMinutes(),
            $matchStats->getMatchDate(),
            $matchStats->isMatchComplete(),
            $homeTeam,
            $awayTeam
        );

        return $match;
    }
}
