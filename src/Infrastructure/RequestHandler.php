<?php

namespace App\Infrastructure;

use Symfony\Component\HttpFoundation\Request;

class RequestHandler
{
    public function createCommand(Request $request, string $commandClassName)
    {
        $values = $this->processRequest($request);

        $command = new $commandClassName();

        foreach ($values as $name => $value) {
            $normalizedName = $this->normalizeKey($name);
            if (property_exists($commandClassName, $normalizedName)) {
                $command->$normalizedName = $value;
            }
        }

        return $command;
    }

    private function processRequest(Request $request): array
    {
        $values = [];

        $requestContent = $request->getContent();

        if (!empty($requestContent)) {
            $values = json_decode($requestContent, true);

            if (json_last_error() != JSON_ERROR_NONE) {
                throw new \InvalidArgumentException(
                    sprintf('%s is not in a valid JSON format', $requestContent)
                );
            }
        }

        //TODO add parsing for request params

        return $values;
    }

    private function normalizeKey(string $propertyName): string
    {
        $camelCasedName = preg_replace_callback('/(^|_|\.)+(.)/', function ($match) {
            return ('.' === $match[1] ? '_' : '') . strtoupper($match[2]);
        }, $propertyName);

        return lcfirst($camelCasedName);
    }
}