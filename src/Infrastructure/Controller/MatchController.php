<?php

namespace App\Infrastructure\Controller;

use App\Application\Command\AcceptMatchCommand;
use App\Application\CommandHandler\AcceptMatchCommandHandler;
use App\Application\Query\GetMatchQuery;
use App\Application\QueryHandler\GetMatchQueryHandler;
use App\Infrastructure\RequestHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MatchController extends AbstractController
{
    public function acceptMatch(Request $request, AcceptMatchCommandHandler $commandHandler)
    {
        $requestHandler = new RequestHandler();
        $command = $requestHandler->createCommand($request, AcceptMatchCommand::class);

        $commandHandler->handle($command);

        return (new Response())->setStatusCode(Response::HTTP_CREATED);
    }

    public function getMatch(string $sportType, string $matchId, GetMatchQueryHandler $getMatchQueryHandler)
    {
        $query = new GetMatchQuery($matchId, $sportType);
        $match = $getMatchQueryHandler->handle($query);

        return $this->json($match);
    }
}