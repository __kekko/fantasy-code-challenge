<?php

namespace App\Infrastructure\Service;

use App\Domain\Player;

class FeedParser
{
    public function parse(Feed $feed): MatchStats
    {
        $feedContent = $feed->getContent();

        $xml = new \SimpleXMLElement($feedContent);

        $isMatchComplete = $this->isMatchComplete($xml);
        $matchLength = $this->getMatchLength($xml);
        $matchDate = $this->getMatchDate($xml);
        $homePlayers = $this->getHomePlayers($xml);
        $awayPlayers = $this->getAwayPlayers($xml);
        $teamNames = $this->getTeamNames($xml);

        return new MatchStats(
            $isMatchComplete,
            $matchLength,
            $matchDate,
            $homePlayers,
            $awayPlayers,
            $teamNames
        );
    }

    private function isMatchComplete(\SimpleXMLElement $xml): bool
    {
        $attributesForMatchResult = $xml->SoccerDocument[0]['Type'];

        if ((string)$attributesForMatchResult === 'Result') {
            return true;
        }

        return false;
    }

    private function getMatchLength(\SimpleXMLElement $xml): int
    {
        return (int)$xml->SoccerDocument->MatchData->Stat[0];
    }

    private function getMatchDate(\SimpleXMLElement $xml): \DateTimeImmutable
    {
        return new \DateTimeImmutable($xml->SoccerDocument->MatchData->MatchInfo->Date);
    }

    private function getHomePlayers(\SimpleXMLElement $xml): array
    {
        $players = [];
        foreach ($xml->SoccerDocument->Team[0]->Player as $player) {
            $players[] = new Player(sprintf('%s %s', $player->PersonName->First, $player->PersonName->Last));
        }

        return $players;
    }

    private function getAwayPlayers(\SimpleXMLElement $xml): array
    {
        $players = [];
        foreach ($xml->SoccerDocument->Team[1]->Player as $player) {
            $players[] = new Player(sprintf('%s %s', $player->PersonName->First, $player->PersonName->Last));
        }

        return $players;
    }

    private function getTeamNames(\SimpleXMLElement $xml): array
    {
        $teamNames['home'] = (string)$xml->SoccerDocument->Team[0]->Name;
        $teamNames['away'] = (string)$xml->SoccerDocument->Team[1]->Name;

        return $teamNames;
    }
}
