<?php

namespace App\Infrastructure\Service;

interface FeedClientInterface
{
    public function retrieveFeed(string $feedUrl): Feed;
}