<?php

namespace App\Infrastructure\Service;

use GuzzleHttp\ClientInterface;

class FeedClient implements FeedClientInterface
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function retrieveFeed(string $feedUrl): Feed
    {
        $response = $this->client->request('GET', $feedUrl);

        return new Feed($response->getBody()->getContents());
    }
}