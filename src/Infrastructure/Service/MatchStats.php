<?php

namespace App\Infrastructure\Service;

class MatchStats
{
    /**
     * @var bool
     */
    private $isMatchComplete;
    /**
     * @var int
     */
    private $matchLengthInMinutes;
    /**
     * @var \DateTimeImmutable
     */
    private $matchDate;
    /**
     * @var array
     */
    private $homePlayers;
    /**
     * @var array
     */
    private $awayPlayers;
    /**
     * @var array
     */
    private $teamNames;

    public function __construct(
        bool $isMatchComplete,
        int $matchLengthInMinutes,
        \DateTimeImmutable $matchDate,
        array $homePlayers,
        array $awayPlayers,
        array $teamNames
    )
    {
        $this->isMatchComplete = $isMatchComplete;
        $this->matchLengthInMinutes = $matchLengthInMinutes;
        $this->matchDate = $matchDate;
        $this->homePlayers = $homePlayers;
        $this->awayPlayers = $awayPlayers;
        $this->teamNames = $teamNames;
    }

    public function isMatchComplete(): bool
    {
        return $this->isMatchComplete;
    }

    public function getMatchLengthInMinutes(): int
    {
        return $this->matchLengthInMinutes;
    }

    public function getMatchDate(): \DateTimeImmutable
    {
        return $this->matchDate;
    }

    public function getHomePlayers(): array
    {
        return $this->homePlayers;
    }

    public function getAwayPlayers(): array
    {
        return $this->awayPlayers;
    }

    public function getTeamNames(): array
    {
        return $this->teamNames;
    }
}