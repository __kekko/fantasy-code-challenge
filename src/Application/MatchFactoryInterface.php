<?php

namespace App\Application;

use App\Application\Command\AcceptMatchCommand;
use App\Domain\Match;

interface MatchFactoryInterface
{
    public function createMatch(AcceptMatchCommand $acceptMatchCommand): Match;
}