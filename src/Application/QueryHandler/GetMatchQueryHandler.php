<?php

namespace App\Application\QueryHandler;

use App\Application\Exception\MatchNotFoundException;
use App\Application\Query\GetMatchQuery;
use App\Domain\Match;
use App\Domain\Repository\MatchRepositoryInterface;

class GetMatchQueryHandler
{
    /**
     * @var MatchRepositoryInterface
     */
    private $matchRepository;

    public function __construct(MatchRepositoryInterface $matchRepository)
    {
        $this->matchRepository = $matchRepository;
    }

    public function handle(GetMatchQuery $getMatchQuery): Match
    {
        $match = $this->matchRepository->findByMatchIdAndSportType($getMatchQuery->sport, $getMatchQuery->matchId);

        if (!$match) {
            throw new MatchNotFoundException();
        }

        return $match;
    }
}