<?php

namespace App\Application\CommandHandler;

use App\Application\Command\AcceptMatchCommand;
use App\Application\MatchFactoryInterface;
use App\Domain\Match;

class AcceptMatchCommandHandler
{
    /**
     * @var MatchFactoryInterface
     */
    private $matchFactory;

    public function __construct(MatchFactoryInterface $matchFactory)
    {
        $this->matchFactory = $matchFactory;
    }

    public function handle(AcceptMatchCommand $command): Match
    {
        $match = $this->matchFactory->createMatch($command);

        return $match;
    }
}