<?php

namespace App\Application\Exception;

class MatchNotFoundException extends \Exception
{
}