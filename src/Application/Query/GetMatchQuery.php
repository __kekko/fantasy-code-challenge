<?php

namespace App\Application\Query;

class GetMatchQuery
{
    /**
     * @var string
     */
    public $matchId;

    /**
     * @var string
     */
    public $sport;

    public function __construct(string $matchId, string $sport)
    {
        $this->matchId = $matchId;
        $this->sport = $sport;
    }
}