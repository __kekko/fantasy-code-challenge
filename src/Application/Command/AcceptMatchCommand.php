<?php

namespace App\Application\Command;

class AcceptMatchCommand
{
    /**
     * @var string
     */
    public $competition;

    /**
     * @var string
     */
    public $matchId;

    /**
     * @var string
     */
    public $season;

    /**
     * @var string
     */
    public $sport;

    /**
     * @var array
     */
    public $teams;

    /**
     * @var \DateTimeImmutable
     */
    public $createdAt;

    /**
     * @var \DateTimeImmutable
     */
    public $updatedAt;

    /**
     * @var string
     */
    public $feedFile;
}