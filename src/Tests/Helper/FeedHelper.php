<?php

namespace App\Tests\Helper;

class FeedHelper
{
    public static function provideFeedContent(): string
    {
        return file_get_contents(__DIR__ . '/../Fixture/match_sample.xml');
    }
}