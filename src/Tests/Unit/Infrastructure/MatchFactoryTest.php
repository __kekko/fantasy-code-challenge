<?php

namespace App\Tests\Unit\Infrastructure;

use App\Application\Command\AcceptMatchCommand;
use App\Domain\Match;
use App\Infrastructure\MatchFactory;
use App\Infrastructure\Service\Feed;
use App\Infrastructure\Service\FeedClientInterface;
use App\Infrastructure\Service\FeedParser;
use App\Infrastructure\Service\MatchStats;
use PHPUnit\Framework\TestCase;

class MatchFactoryTest extends TestCase
{
    public function test_create_match()
    {
        $command = $this->getCommand();
        $feed = new Feed('feed content');
        $matchStats = new MatchStats(
            true,
            92,
            new \DateTimeImmutable(),
            [],
            [],
            [
                'home' => 'Foo',
                'away' => 'Bar',
            ]
        );

        /** @var FeedClientInterface $feedClient */
        $feedClient = $this->prophesize(FeedClientInterface::class);
        $feedClient->retrieveFeed($command->feedFile)->shouldBeCalled()->willReturn($feed);

        /** @var FeedParser $feedParser */
        $feedParser = $this->prophesize(FeedParser::class);
        $feedParser->parse($feed)->shouldBeCalled()->willReturn($matchStats);

        $factory = new MatchFactory($feedClient->reveal(), $feedParser->reveal());

        $match = $factory->createMatch($command);

        self::assertInstanceOf(Match::class, $match);
    }

    private function getCommand(): AcceptMatchCommand
    {
        $command = new AcceptMatchCommand();

        $command->competition = 'competition name';
        $command->matchId = 'match id';
        $command->season = 'season';
        $command->sport = 'sport';
        $command->teams = ['home' => 'home team name', 'away' => 'away team name'];
        $command->createdAt = new \DateTimeImmutable();
        $command->updatedAt = new \DateTimeImmutable();
        $command->feedFile = 'feed file url';

        return $command;
    }
}