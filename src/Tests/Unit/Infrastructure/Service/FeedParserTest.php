<?php

namespace App\Tests\Unit\Infrastructure\Service;

use App\Infrastructure\Service\Feed;
use App\Infrastructure\Service\FeedParser;
use App\Tests\Helper\FeedHelper;
use PHPUnit\Framework\TestCase;

class FeedParserTest extends TestCase
{
    public function test_parse_feed()
    {
        $feed = new Feed(FeedHelper::provideFeedContent());

        $feedParser = new FeedParser();
        $matchStats = $feedParser->parse($feed);

        self::assertEquals(96, $matchStats->getMatchLengthInMinutes());
        self::assertTrue($matchStats->isMatchComplete());
    }
}