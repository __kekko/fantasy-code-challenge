<?php

namespace App\Tests\Unit\Infrastructure\Service;

use App\Infrastructure\Service\Feed;
use App\Infrastructure\Service\FeedClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class FeedClientTest extends TestCase
{
    public function test_retrieve_feed_should_return_a_feed()
    {
        /** @var ClientInterface $client */
        $client = $this->prophesize(ClientInterface::class);

        $client->request('GET', 'feed url')->shouldBeCalled()->willReturn(new Response());

        $feedClient = new FeedClient($client->reveal());
        $feed = $feedClient->retrieveFeed('feed url');

        self::assertInstanceOf(Feed::class, $feed);
    }
}
