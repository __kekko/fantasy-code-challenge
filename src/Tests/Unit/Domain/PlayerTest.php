<?php

namespace App\Tests\Unit\Domain;

use App\Domain\Player;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    public function test_can_score_a_goal()
    {
        $player = new Player('Filippo Inzaghi');

        self::assertEquals(0, $player->getTotalScoredGoal());

        $player->scoreGoal();

        self::assertEquals(1, $player->getTotalScoredGoal());
    }

    public function test_can_get_a_red_card()
    {
        $player = new Player('Roberto Carlos');

        $player->getARedCard();

        self::assertTrue($player->hasGotARedCard());
    }

    /**
     * @expectedException \App\Domain\Exception\RedCardAlreadyGotException
     */
    public function test_cannot_get_more_than_one_red_card()
    {
        $player = new Player('Roberto Carlos');

        $player->getARedCard();
        $player->getARedCard();
    }

    public function test_can_get_a_yellow_card()
    {
        $player = new Player('Luigi Di Biagio');

        $player->getAYellowCard();

        self::assertTrue($player->hasGotAYellowCard());
    }

    public function test_can_get_2_yellow_cards_and_get_a_red_card()
    {
        $player = new Player('Luigi Di Biagio');

        $player->getAYellowCard();
        $player->getAYellowCard();

        self::assertTrue($player->hasGotAYellowCard());
        self::assertTrue($player->hasGotARedCard());
    }

    /**
     * @expectedException \App\Domain\Exception\YellowCardsAlreadyGotException
     */
    public function test_cannot_get_more_than_2_yellow_cards()
    {
        $player = new Player('Luigi Di Biagio');

        $player->getAYellowCard();
        $player->getAYellowCard();
        $player->getAYellowCard();

        self::assertTrue($player->hasGotAYellowCard());
    }

    public function test_can_perform_a_tackle()
    {
        $player = new Player('Ronaldo');

        $player->performTackle();
        $player->performTackle();

        self::assertEquals(2, $player->getTotalTackles());
    }

    public function test_can_perform_a_foul()
    {
        $player = new Player('Ronaldo');

        $player->performFoul();
        $player->performFoul();

        self::assertEquals(2, $player->getTotalFouls());
    }

    public function test_can_perform_a_touche()
    {
        $player = new Player('Ronaldo');

        $player->performTouche();
        $player->performTouche();

        self::assertEquals(2, $player->getTotalTouches());
    }
}