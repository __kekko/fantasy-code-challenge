<?php

namespace App\Tests\Unit\Domain;

use App\Domain\Match;
use App\Domain\Player;
use App\Domain\Team;
use PHPUnit\Framework\TestCase;

class MatchTest extends TestCase
{
    public function test_calculate_top_scorer()
    {
        /** @var Player[] $homePlayers */
        $playersHome = [
            new Player('Francesco Totti'),
            new Player('Alessandro Del Piero'),
            new Player('Gennaro Gattuso'),
        ];

        $playersHome[0]->scoreGoal();
        $playersHome[0]->scoreGoal();
        $playersHome[0]->scoreGoal();

        /** @var Player[] $awayPlayers */
        $playersAway = [
            new Player('David Beckham'),
            new Player('Eric Cantona'),
            new Player('Armando Maradona'),
        ];

        $playersAway[1]->scoreGoal();
        $playersAway[1]->scoreGoal();

        $homeTeam = new Team('AS Roma', $playersHome);
        $awayTeam = new Team('AC Milan', $playersAway);

        $match = $this->createMatch($homeTeam, $awayTeam);

        $topScorer = $match->calculateTopScorer();

        self::assertEquals('Francesco Totti', $topScorer->getName());
    }

    public function test_calculate_top_scorer_when_no_one_scored()
    {
        /** @var Player[] $homePlayers */
        $playersHome = [
            new Player('Alessandro Del Piero'),
            new Player('Francesco Totti'),
            new Player('Gennaro Gattuso'),
        ];

        /** @var Player[] $awayPlayers */
        $playersAway = [
            new Player('David Beckham'),
            new Player('Eric Cantona'),
            new Player('Armando Maradona'),
        ];

        $homeTeam = new Team('AS Roma', $playersHome);
        $awayTeam = new Team('AC Milan', $playersAway);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertNull($match->calculateTopScorer());
    }

    public function test_calculate_winner_team()
    {
        /** @var Player[] $homePlayers */
        $playersHome = [
            new Player('Alessandro Del Piero'),
            new Player('Francesco Totti'),
            new Player('Gennaro Gattuso'),
        ];

        $playersHome[0]->scoreGoal();
        $playersHome[0]->scoreGoal();

        /** @var Player[] $awayPlayers */
        $playersAway = [
            new Player('David Beckham'),
            new Player('Eric Cantona'),
            new Player('Armando Maradona'),
        ];

        $playersAway[0]->scoreGoal();

        $homeTeam = new Team('AS Roma', $playersHome);
        $awayTeam = new Team('AC Milan', $playersAway);

        $match = $this->createMatch($homeTeam, $awayTeam);

        $winnerTeam = $match->calculateWinnerTeam();

        self::assertEquals('AS Roma', $winnerTeam->getName());
    }

    public function test_calculate_winner_team_when_it_is_a_draw()
    {
        /** @var Player[] $homePlayers */
        $playersHome = [
            new Player('Alessandro Del Piero'),
            new Player('Francesco Totti'),
            new Player('Gennaro Gattuso'),
        ];

        /** @var Player[] $awayPlayers */
        $playersAway = [
            new Player('David Beckham'),
            new Player('Eric Cantona'),
            new Player('Armando Maradona'),
        ];

        $homeTeam = new Team('AS Roma', $playersHome);
        $awayTeam = new Team('AC Milan', $playersAway);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertNull($match->calculateWinnerTeam());
    }

    public function test_calculate_total_goals()
    {
        /** @var Player[] $homePlayers */
        $homePlayers = [
            new Player('Falcao'),
            new Player('Romario')
        ];

        $homePlayers[0]->scoreGoal();

        /** @var Player[] $awayPlayers */
        $awayPlayers = [
            new Player('Pelè'),
            new Player('Garrincha')
        ];

        $awayPlayers[0]->scoreGoal();
        $awayPlayers[1]->scoreGoal();

        $homeTeam = new Team('FC Barcelona', $homePlayers);
        $awayTeam = new Team('Atletico Madrid', $awayPlayers);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertEquals(3, $match->calculateTotalGoals());
    }

    public function test_calculate_total_red_cards()
    {
        /** @var Player[] $homePlayers */
        $homePlayers = [
            new Player('Falcao'),
            new Player('Romario')
        ];

        $homePlayers[0]->getARedCard();

        /** @var Player[] $awayPlayers */
        $awayPlayers = [
            new Player('Pelè'),
            new Player('Garrincha')
        ];

        $awayPlayers[0]->getARedCard();
        $awayPlayers[1]->getARedCard();

        $homeTeam = new Team('FC Barcelona', $homePlayers);
        $awayTeam = new Team('Atletico Madrid', $awayPlayers);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertEquals(3, $match->calculateTotalRedCards());
    }

    public function test_calculate_total_yellow_cards()
    {
        /** @var Player[] $homePlayers */
        $homePlayers = [
            new Player('Falcao'),
            new Player('Romario')
        ];

        $homePlayers[0]->getAYellowCard();
        $homePlayers[0]->getAYellowCard();

        /** @var Player[] $awayPlayers */
        $awayPlayers = [
            new Player('Pelè'),
            new Player('Garrincha')
        ];

        $awayPlayers[0]->getAYellowCard();
        $awayPlayers[1]->getAYellowCard();

        $homeTeam = new Team('FC Barcelona', $homePlayers);
        $awayTeam = new Team('Atletico Madrid', $awayPlayers);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertEquals(4, $match->calculateTotalYellowCards());
    }

    public function test_calculate_total_number_of_home_and_away_team_tackles()
    {
        /** @var Player[] $homePlayers */
        $homePlayers = [
            new Player('Falcao'),
            new Player('Romario')
        ];

        $homePlayers[0]->performTackle();
        $homePlayers[0]->performTackle();
        $homePlayers[1]->performTackle();

        /** @var Player[] $awayPlayers */
        $awayPlayers = [
            new Player('Pelè'),
            new Player('Garrincha')
        ];

        $awayPlayers[1]->performTackle();

        $homeTeam = new Team('FC Barcelona', $homePlayers);
        $awayTeam = new Team('Atletico Madrid', $awayPlayers);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertEquals(3, $match->calculateTotalTacklesForHomeTeam());
        self::assertEquals(1, $match->calculateTotalTacklesForAwayTeam());
    }

    public function test_calculate_total_number_of_home_and_away_team_fouls()
    {
        /** @var Player[] $homePlayers */
        $homePlayers = [
            new Player('Falcao'),
            new Player('Romario')
        ];

        $homePlayers[0]->performFoul();
        $homePlayers[0]->performFoul();
        $homePlayers[1]->performFoul();

        /** @var Player[] $awayPlayers */
        $awayPlayers = [
            new Player('Pelè'),
            new Player('Garrincha')
        ];

        $awayPlayers[1]->performFoul();

        $homeTeam = new Team('FC Barcelona', $homePlayers);
        $awayTeam = new Team('Atletico Madrid', $awayPlayers);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertEquals(3, $match->calculateTotalFoulsForHomeTeam());
        self::assertEquals(1, $match->calculateTotalFoulsForAwayTeam());
    }

    public function test_calculate_total_number_of_home_and_away_team_touches()
    {
        /** @var Player[] $homePlayers */
        $homePlayers = [
            new Player('Falcao'),
            new Player('Romario')
        ];

        $homePlayers[0]->performTouche();
        $homePlayers[0]->performTouche();
        $homePlayers[1]->performTouche();

        /** @var Player[] $awayPlayers */
        $awayPlayers = [
            new Player('Pelè'),
            new Player('Garrincha')
        ];

        $awayPlayers[1]->performTouche();
        $awayPlayers[1]->performTouche();

        $homeTeam = new Team('FC Barcelona', $homePlayers);
        $awayTeam = new Team('Atletico Madrid', $awayPlayers);

        $match = $this->createMatch($homeTeam, $awayTeam);

        self::assertEquals(3, $match->calculateTotalTouchesForHomeTeam());
        self::assertEquals(2, $match->calculateTotalTouchesForAwayTeam());
    }

    private function createMatch($homeTeam, $awayTeam): Match
    {
        return new Match(
            'competicion',
            'match_id',
            'season',
            'sport',
            91,
            new \DateTimeImmutable(),
            true,
            $homeTeam,
            $awayTeam
        );
    }
}