<?php

namespace App\Tests\Unit\Domain;

use App\Domain\Player;
use App\Domain\Team;
use PHPUnit\Framework\TestCase;

class TeamTest extends TestCase
{
    public function test_can_be_initialised()
    {
        $team = new Team('AS Roma', []);

        self::assertInstanceOf(Team::class, $team);
    }

    public function test_calculate_number_of_goal_scored()
    {
        $player1 = new Player('Francesco Totti');
        $player2 = new Player('Alessandro Del Piero');
        $player3 = new Player('Gennaro Gattuso');

        $player1->scoreGoal();
        $player1->scoreGoal();
        $player3->scoreGoal();

        $team = new Team('AS Roma', [$player1, $player2, $player3]);
        $goalScored = $team->calculateNumberOfGoalScored();

        self::assertEquals(3, $goalScored);
    }

    public function test_calculate_number_of_red_cards()
    {
        $player1 = new Player('Francesco Totti');
        $player2 = new Player('Alessandro Del Piero');
        $player3 = new Player('Gennaro Gattuso');

        $player1->getARedCard();
        $player2->getARedCard();

        $team = new Team('AS Roma', [$player1, $player2, $player3]);
        $redCards = $team->calculateTotalRedCards();

        self::assertEquals(2, $redCards);
    }

    public function test_calculate_number_of_yellow_cards()
    {
        $player1 = new Player('Francesco Totti');
        $player2 = new Player('Alessandro Del Piero');
        $player3 = new Player('Gennaro Gattuso');

        $player1->getAYellowCard();
        $player1->getAYellowCard();
        $player2->getARedCard();
        $player3->getAYellowCard();

        $team = new Team('AS Roma', [$player1, $player2, $player3]);
        $yellowCards = $team->calculateTotalYellowCards();

        self::assertEquals(3, $yellowCards);
    }

    public function test_calculate_total_number_of_tackles()
    {
        $player1 = new Player('Francesco Totti');
        $player2 = new Player('Alessandro Del Piero');
        $player3 = new Player('Gennaro Gattuso');

        $player1->performTackle();
        $player1->performTackle();
        $player1->performTackle();
        $player3->performTackle();

        $team = new Team('AS Roma', [$player1, $player2, $player3]);
        $tackles = $team->calculateTotalNumberOfTackles();

        self::assertEquals(4, $tackles);
    }

    public function test_calculate_total_number_of_fouls()
    {
        $player1 = new Player('Francesco Totti');
        $player2 = new Player('Alessandro Del Piero');
        $player3 = new Player('Gennaro Gattuso');

        $player1->performFoul();
        $player1->performFoul();
        $player3->performFoul();

        $team = new Team('AS Roma', [$player1, $player2, $player3]);
        $fouls = $team->calculateTotalNumberOfFouls();

        self::assertEquals(3, $fouls);
    }

    public function test_calculate_total_number_of_touches()
    {
        $player1 = new Player('Francesco Totti');
        $player2 = new Player('Alessandro Del Piero');
        $player3 = new Player('Gennaro Gattuso');

        $player1->performTouche();
        $player1->performTouche();
        $player3->performTouche();

        $team = new Team('AS Roma', [$player1, $player2, $player3]);
        $touches = $team->calculateTotalNumberOfTouches();

        self::assertEquals(3, $touches);
    }
}
