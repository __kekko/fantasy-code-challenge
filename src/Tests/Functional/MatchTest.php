<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MatchTest extends WebTestCase
{
    public function test_posting_a__match_returns_201()
    {
        $client = static::createClient();
        $client->request('POST', '/match', [], [], [], $this->getMatchInJsonFormat());

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    /**
     * @expectedException \App\Application\Exception\MatchNotFoundException
     */
    public function test_retrieve_a_match()
    {
        $client = static::createClient();
        $client->request('GET', '/match-data/football/123');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    private function getMatchInJsonFormat(): string
    {
        return
            <<<JSON
        {
          "competition": "English Barclays Premier League",
          "match_id": 2723,
          "season": 2017,
          "sport": "football",
          "teams": {
            "home": "Arsenal",
            "away": "Leicester City"
          },
          "created_at": "2018-01-08 15:24:20",
          "updated_at": "2018-01-08 15:24:20",
          "feed_file": "https://realtimematches.com/feed/football/2723-489337.xml"
        }
JSON;
    }
}
