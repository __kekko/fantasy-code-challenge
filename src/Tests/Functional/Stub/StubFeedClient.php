<?php

namespace App\Tests\Functional\Stub;

use App\Infrastructure\Service\Feed;
use App\Infrastructure\Service\FeedClientInterface;
use App\Tests\Helper\FeedHelper;

class StubFeedClient implements FeedClientInterface
{
    public function retrieveFeed(string $feedUrl): Feed
    {
        return new Feed(FeedHelper::provideFeedContent());
    }
}