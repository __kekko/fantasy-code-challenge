<?php

namespace App\Domain\Repository;

use App\Domain\Match;

interface MatchRepositoryInterface
{
    public function save(Match $match): void;

    public function findByMatchIdAndSportType(string $sportType, string $matchId): ?Match;
}