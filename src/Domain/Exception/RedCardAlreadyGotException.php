<?php

namespace App\Domain\Exception;

class RedCardAlreadyGotException extends \DomainException
{
}