<?php

namespace App\Domain;

class Team
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Player[]
     */
    private $players;

    public function __construct(string $name, array $players)
    {
        $this->name = $name;
        $this->players = $players;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPlayers(): array
    {
        return $this->players;
    }

    public function calculateNumberOfGoalScored(): int
    {
        $totalGoals = array_reduce(
            $this->players,
            function ($totalGoalScored, $player) {
                /** @var $player Player */
                $totalGoalScored += $player->getTotalScoredGoal();

                return $totalGoalScored;
            },
            0
        );

        return $totalGoals;
    }

    public function calculateTotalRedCards(): int
    {
        $numberOfRedCards = 0;

        foreach ($this->players as $player) {
            if ($player->hasGotARedCard()) {
                $numberOfRedCards++;
            }
        }

        return $numberOfRedCards;
    }

    public function calculateTotalYellowCards(): int
    {
        $numberOfYellowCards = 0;

        foreach ($this->players as $player) {
            if ($player->hasGotAYellowCard()) {
                $numberOfYellowCards += $player->getNumberOfYellowCards();
            }
        }

        return $numberOfYellowCards;
    }

    public function calculateTotalNumberOfTackles(): int
    {
        $totalTackles = 0;

        foreach ($this->players as $player) {
            $totalTackles += $player->getTotalTackles();
        }

        return $totalTackles;
    }

    public function calculateTotalNumberOfFouls(): int
    {
        $totalFouls = 0;

        foreach ($this->players as $player) {
            $totalFouls += $player->getTotalFouls();
        }

        return $totalFouls;
    }

    public function calculateTotalNumberOfTouches(): int
    {
        $totalTouches = 0;

        foreach ($this->players as $player) {
            $totalTouches += $player->getTotalTouches();
        }

        return $totalTouches;
    }
}