<?php

namespace App\Domain;

class Match implements \JsonSerializable
{
    /**
     * @var Team
     */
    private $homeTeam;

    /**
     * @var Team
     */
    private $awayTeam;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;
    /**
     * @var string
     */
    private $competition;
    /**
     * @var string
     */
    private $matchId;
    /**
     * @var string
     */
    private $season;
    /**
     * @var string
     */
    private $sport;
    /**
     * @var int
     */
    private $matchLength;
    /**
     * @var \DateTimeImmutable
     */
    private $matchDate;
    /**
     * @var bool
     */
    private $completed;

    public function __construct(
        string $competition,
        string $matchId,
        string $season,
        string $sport,
        int $matchLength,
        \DateTimeImmutable $matchDate,
        bool $completed,
        Team $homeTeam,
        Team $awayTeam
    )
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->homeTeam = $homeTeam;
        $this->awayTeam = $awayTeam;
        $this->competition = $competition;
        $this->matchId = $matchId;
        $this->season = $season;
        $this->sport = $sport;
        $this->matchLength = $matchLength;
        $this->matchDate = $matchDate;
        $this->completed = $completed;
    }

    public function calculateTopScorer(): ?Player
    {
        $matchPlayers = array_merge($this->homeTeam->getPlayers(), $this->awayTeam->getPlayers());

        $maxGoalScored = 0;
        $currentTopScorer = null;

        foreach ($matchPlayers as $matchPlayer) {
            /** @var $matchPlayer Player */
            if ($matchPlayer->getTotalScoredGoal() > $maxGoalScored) {
                $currentTopScorer = $matchPlayer;
                $maxGoalScored = $matchPlayer->getTotalScoredGoal();
            }
        }

        return $currentTopScorer;
    }

    public function calculateWinnerTeam(): ?Team
    {
        if ($this->homeTeam->calculateNumberOfGoalScored() > $this->awayTeam->calculateNumberOfGoalScored()) {
            return $this->homeTeam;
        }

        if ($this->homeTeam->calculateNumberOfGoalScored() < $this->awayTeam->calculateNumberOfGoalScored()) {
            return $this->awayTeam;
        }

        return null;
    }

    public function calculateTotalGoals(): int
    {
        return $this->homeTeam->calculateNumberOfGoalScored() + $this->awayTeam->calculateNumberOfGoalScored();
    }

    public function calculateTotalRedCards(): int
    {
        return $this->homeTeam->calculateTotalRedCards() + $this->awayTeam->calculateTotalRedCards();
    }

    public function calculateTotalYellowCards(): int
    {
        return $this->homeTeam->calculateTotalYellowCards() + $this->awayTeam->calculateTotalYellowCards();
    }

    public function calculateTotalTacklesForHomeTeam(): int
    {
        return $this->homeTeam->calculateTotalNumberOfTackles();
    }

    public function calculateTotalTacklesForAwayTeam(): int
    {
        return $this->awayTeam->calculateTotalNumberOfTackles();
    }

    public function calculateTotalFoulsForHomeTeam(): int
    {
        return $this->homeTeam->calculateTotalNumberOfFouls();
    }

    public function calculateTotalFoulsForAwayTeam(): int
    {
        return $this->awayTeam->calculateTotalNumberOfFouls();
    }

    public function calculateTotalTouchesForHomeTeam(): int
    {
        return $this->homeTeam->calculateTotalNumberOfTouches();
    }

    public function calculateTotalTouchesForAwayTeam(): int
    {
        return $this->awayTeam->calculateTotalNumberOfTouches();
    }

    public function jsonSerialize()
    {
        return [
            'competition' => $this->competition,
            'match_id' => $this->matchId,
            'season' => $this->season,
            'sport' => $this->sport,
            'teams' =>
                [
                    'home' => $this->homeTeam->getName(),
                    'away' => $this->awayTeam->getName(),
                ],
            'match_length' => $this->matchLength,
            'match_date' => $this->matchDate,
            'created_at' => $this->createdAt,
            'updated_at' => $this->createdAt,
            'complete' => $this->completed,
            'stats' =>
                [
                    'top_scorer' => $this->calculateTopScorer(),
                    'winner' => $this->calculateWinnerTeam(),
                    'total_goals' => $this->calculateTotalGoals(),
                    'red_cards' => $this->calculateTotalRedCards(),
                    'yellow_cards' => $this->calculateTotalYellowCards(),
                    'home' =>
                        [
                            'total_tackles' => $this->homeTeam->calculateTotalNumberOfTackles(),
                            'total_touches' => $this->homeTeam->calculateTotalNumberOfTouches(),
                            'total_fouls' => $this->homeTeam->calculateTotalNumberOfFouls(),
                        ],
                    'away' =>
                        [
                            'total_tackles' => $this->awayTeam->calculateTotalNumberOfTackles(),
                            'total_touches' => $this->awayTeam->calculateTotalNumberOfTouches(),
                            'total_fouls' => $this->awayTeam->calculateTotalNumberOfFouls(),
                        ],
                ],
        ];
    }
}
