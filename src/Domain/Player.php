<?php

namespace App\Domain;

use App\Domain\Exception\RedCardAlreadyGotException;
use App\Domain\Exception\YellowCardsAlreadyGotException;

class Player
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $goalScored;

    /**
     * @var bool
     */
    private $redCard;

    /**
     * @var int
     */
    private $yellowCards;

    /**
     * @var int
     */
    private $tackles;

    /**
     * @var int
     */
    private $fouls;

    /**
     * @var int
     */
    private $touches;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->goalScored = 0;
        $this->redCard = false;
        $this->yellowCards = 0;
        $this->tackles = 0;
        $this->fouls = 0;
        $this->touches = 0;
    }

    public function scoreGoal(): void
    {
        $this->goalScored += 1;
    }

    public function getTotalScoredGoal(): int
    {
        return $this->goalScored;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getARedCard(): void
    {
        if ($this->hasGotARedCard()) {
            throw new RedCardAlreadyGotException();
        }

        $this->redCard = true;
    }

    public function hasGotARedCard(): bool
    {
        return $this->redCard;
    }

    public function getAYellowCard(): void
    {
        if ($this->hasGot2YellowCards()) {
            throw new YellowCardsAlreadyGotException();
        }

        $this->yellowCards++;

        if ($this->hasGot2YellowCards()) {
            $this->getARedCard();
        }
    }

    public function hasGotAYellowCard(): bool
    {
        return $this->yellowCards > 0;
    }

    private function hasGot2YellowCards(): bool
    {
        return $this->yellowCards === 2;
    }

    public function getNumberOfYellowCards(): int
    {
        return $this->yellowCards;
    }

    public function performTackle(): void
    {
        $this->tackles++;
    }

    public function getTotalTackles(): int
    {
        return $this->tackles;
    }

    public function performFoul(): void
    {
        $this->fouls++;
    }

    public function getTotalFouls(): int
    {
        return $this->fouls;
    }

    public function performTouche(): void
    {
        $this->touches++;
    }

    public function getTotalTouches(): int
    {
        return $this->touches;
    }
}