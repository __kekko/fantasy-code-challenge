# Fantasy Code Challenge

[Code Challenge](https://bitbucket.org/fantasydevelopment/backend-technical-test/src)

I decided to start writing the solution to this challenge using a 
[Rich Modeling](https://www.martinfowler.com/bliki/AnemicDomainModel.html) approach because I think that 
promotes the reusability of the code and allows to defer the choice of implementation details. 
It also improves the understandability of the domain simply reading the code (working as a self internal documentation).
This means that I started writing code without _solutionising_ any technical detail
(like DB, framework, protocols, clients, etc.) and I focused on representing the 
domain behaviours in the code (the Model).
All it matters, in this initial phase, is to give the domain a representation in form of code.

Once I have completed this part, I proceeded implementing the outer parts of the codebase.
In this specific challenge, there will be only 2 use-cases (post a match and retrieve a match).
Every use-case is represented by a `CommandHandler` that will orchestrate the model and 
make it act as expected for the specific business case.
It's common practice to keep this layer (often called **application layer**) very thin because the only 
responsibility is to make the model "_act_".

The last part is the **infrastructure layer**, that contains all the details that are irrelevant 
for the domain. This part contains the controllers, framework, and db repositories implementations.

They way these parts stay separated is using [IoC](https://en.wikipedia.org/wiki/Inversion_of_control).


#N.B. The test is incomplete
The challenge, in my opinion, takes too long to be just a technical assessment. So what I did
is basically giving the structure of the solution that I would have implemented.

Missing parts:
 - There is not implementation for the persistence layer (so no DB, no ORM)
 - The endpoint to retrieve the match is not working. It throws a `MatchNotFoundException`.
 - The endpoint for accepting the match is not persisting the Match.
 - The logic to parse some of the aggregate fields (like number of yellow/red cards) has not been implemented 
   in the Infrastructure layer. The logic is only at the domain level. 

I know that this is not meeting the requirements of the test, and I understand if you 
would consider to reject the test because incomplete.
 
### Run tests
```bin/phpunit```